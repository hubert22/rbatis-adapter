# rbatis-adapter

This crate is extended by diesel-adapter.

Rbatis Adapter is the [Rbatis](https://rbatis.github.io/rbatis.io) adapter
for [Casbin-rs](https://github.com/casbin/casbin-rs). With this library, Casbin can load policy from Rbatis supported
database or save policy to it.

Based on [Rbatis](https://rbatis.github.io/rbatis.io), The current supported databases are:

- [Mysql](https://www.mysql.com/)
- [Postgres](https://github.com/lib/pq)
- [SQLite](https://www.sqlite.org)

## Notice

In order to unify the database table name in Casbin ecosystem, we decide to use `casbin_rule` instead of `casbin_rules`
from version `0.0.1`. If you are using old version `rbatis-adapter` in your production environment, please use following
command and update `rbatis-adapter` version:

[//]: # (````SQL)

[//]: # (# MySQL & PostgreSQL & SQLite)

[//]: # (ALTER TABLE casbin_rules RENAME TO casbin_rule;)

[//]: # (````)

[//]: # (## Install)

[//]: # ()

[//]: # (Add it to `Cargo.toml`)

[//]: # ()

[//]: # (```)

[//]: # (rbatis-adapter = { version = "0.9.0", features = ["postgres"] })

[//]: # (tokio = { version = "1.1.1", features = ["macros", "rt-multi-thread"] })

[//]: # (```)