use std::{error::Error as StdError, fmt};

#[derive(Debug)]
pub enum Error {
    Rbatis(rbatis::Error),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Error::*;

        match self {
            Rbatis(err) => err.fmt(f),
        }
    }
}

impl StdError for Error {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        use Error::*;

        match self {
            Rbatis(err) => Some(err),
        }
    }
}

impl From<rbatis::Error> for Error {
    fn from(err: rbatis::Error) -> Self {
        Error::Rbatis(err)
    }
}
