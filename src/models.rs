#[crud_table(table_name: casbin_rule)]
#[derive(Clone, Debug)]
pub(crate) struct CasbinRule {
    pub id: Option<i32>,
    pub ptype: String,
    pub v0: String,
    pub v1: String,
    pub v2: String,
    pub v3: String,
    pub v4: String,
    pub v5: String,
}

// #[derive(Insertable, Clone)]
// #[table_name = "casbin_rule"]
// pub(crate) struct NewCasbinRule {
//     pub ptype: String,
//     pub v0: String,
//     pub v1: String,
//     pub v2: String,
//     pub v3: String,
//     pub v4: String,
//     pub v5: String,
// }
